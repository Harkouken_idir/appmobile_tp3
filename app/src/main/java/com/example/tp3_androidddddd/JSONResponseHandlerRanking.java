package com.example.tp3_androidddddd;

import android.util.JsonReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Process the response to a GET request to the Web service
 * https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l=4414&s=1920
 * Responses must be provided in JSON.
 *
 */


public class JSONResponseHandlerRanking {

    private static final String TAG = JSONResponseHandlerRanking.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerRanking(Team team) {
        this.team = team;
    }

    /**
     * @param response done by the Web service
     * @return A Team with attributes filled with the collected information if response was
     * successfully analyzed
     */

    public void readJsonStreamRank(InputStream response) throws IOException {

        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));

        try {
            readRank(reader);
        } finally {
            reader.close();
        }
    }

    public void readRank(JsonReader reader) throws IOException {

        reader.beginObject();

        while (reader.hasNext()) {

            String name = reader.nextName();

            if (name.equals("table")) {
                readArrayRank(reader);
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();
    }

    private void readArrayRank(JsonReader reader) throws IOException {

        reader.beginArray();

        int index = 0;

        // parcourir les team
        while (reader.hasNext() ) {

            reader.beginObject();

            ++index;

            while (reader.hasNext()) {
                String name = reader.nextName();

                if (name != null && name.equals("name")) {

                    String teamName = reader.nextString();
                    String currentTeamName = team.getName();

                    if (teamName != null && teamName.equals(currentTeamName)) {
                        team.setRanking(index);
                    }

                    while (reader.hasNext()) {
                        String nameBis = reader.nextName();

                        if (nameBis.equals("total")) {
                            team.setTotalPoints(reader.nextInt());
                            break;
                        } else {
                            reader.skipValue();
                        }
                    }

                } else {

                    if (reader.hasNext()) {
                        reader.skipValue();
                    }
                }
            }

            reader.endObject();
        }

        reader.endArray();
    }



}
